from datetime import timedelta


class Config:
    SECRET_KEY = 'Secret'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=100)
    JWT_SECRET_KEY = 'JWT_SECRET_KEY'
    JWT_TOKEN_LOCATION = ["cookies"]


class DevConfig(Config):
    JWT_COOKIE_SECURE = False
