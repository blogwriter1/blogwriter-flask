FROM python:3.10.6

WORKDIR /app

COPY ./requirements.txt /app
RUN pip install --no-cache-dir --upgrade -r requirements.txt

copy . /app

CMD ["flask", "--app", "blogwriter", "run", "--debug", "--host", "0.0.0.0"]