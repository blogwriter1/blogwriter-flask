from flask import (
    Blueprint,
    request,
    jsonify,
)
from firebase_admin._user_mgt import UserRecord
from blogwriter import db
from blogwriter.utils.firebase_helper import jwt_required
from blogwriter.models.user import User
from blogwriter.models.post import Post


bp = Blueprint('blog', __name__)


@bp.get("/blog")
@jwt_required
def blog(firebase_user: UserRecord):

    user: User = User.query.filter_by(email=firebase_user.email).first()
    posts: Post = Post.query.filter_by(user_id=user.id)

    res = []
    for post in reversed(list(posts)):
        res.append(
            {
                "id": post.id,
                "title": post.title,
                "content": post.content
            }
        )

    return res, 200


@bp.delete("/blog/<blogId>")
@jwt_required
def deleteBlog(user: UserRecord, blogId: int):
    Post.query.filter_by(id=blogId).delete()
    db.session.commit()
    res = {
        "id": blogId,
        "result": "delete successful"
    }
    return res, 200
