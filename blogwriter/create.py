import logging
from flask import (
    Blueprint,
    request,
)
from firebase_admin._user_mgt import UserRecord
from blogwriter import db
from blogwriter.models.user import User
from blogwriter.models.post import Post
from blogwriter.utils.chatgpt import chatgpt
from blogwriter.utils.firebase_helper import jwt_required


bp = Blueprint('blogwriter', __name__)


@bp.post("/create")
@jwt_required
def create(firebase_user: UserRecord):
    req_data: dict = request.get_json()
    questions: list = req_data.get("questions")
    answers: list = req_data.get("answers")
    print(questions)
    print(answers)
    q_a = ""
    for q, a in zip(questions, answers):
        q_a += f"{q}\n{a}\n\n"

    user: User = User.query.filter_by(email=firebase_user.email).first()

    content: str = '''
Q: I would like to write an article today.
A: """ great, what can I help you with? """
Q: First, I want to make sure that all the answers will be enclosed in """,
do you understand?
A: """"yes, I understand, you want the answers to be enclosed in
triple quotes. """
I have my clients anwswering the following questions:
%s
Based on these information, can you generate a blog using markdown that
encolses in """
A:
''' % (q_a)
    message = [
        {"role": "system", "content": "You are an expert blog writer"},
        {"role": "user", "content": content}
    ]

    logging.info("====chatgpt content:====\n" + content + "\n======")
    title: str = "No Title"
    result: str = chatgpt(message)
    msg_parts = result.split("\n")
    for msg in msg_parts:
        if "#" not in msg:
            continue
        title: str = msg.split("#")[1].strip()
        break

    result = result.split('"""')
    result = result[1].strip()
    post = Post(title=title, content=result, user_id=user.id)
    db.session.add(post)
    db.session.commit()

    return {"response": "success"}, 201
