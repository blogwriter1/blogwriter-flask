import os
import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_jwt_extended import JWTManager
import firebase_admin
from firebase_admin import credentials


logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(message)s')

db = SQLAlchemy()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        SQLALCHEMY_DATABASE_URI="sqlite:///blogwriter.sqlite",
    )

    app.config.from_object('config.DevConfig')

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Setup the Flask-JWT-Extended extension
    JWTManager(app)

    sdk_file = os.path.join(
        app.instance_path, "blogwriter-firebase-adminsdk.json")
    cred = credentials.Certificate(sdk_file)

    firebase_admin.initialize_app(cred)

    # enable CORS
    # CORS(app, resources={r'/*': {'origins': 'http://localhost:8080'}},
    #      supports_credentials=True)
    CORS(app, resources={r'/*': {"origins": "*"}},
         supports_credentials=True)
    # CORS(app, resources={r'/*': {"origins": "*"}})

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    from . import auth, blog, create, publish
    app.register_blueprint(auth.bp)
    app.register_blueprint(blog.bp)
    app.register_blueprint(create.bp)
    app.register_blueprint(publish.bp)

    from . import db
    db.init_app(app)

    return app
