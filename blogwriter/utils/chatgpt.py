import os
import logging
from typing import List
import openai


openai.api_key = os.environ.get("CHATGPT_KEY")


def chatgpt(msg_history: List[dict]) -> str:
    logging.info("generating blog in process...")
    completion = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=msg_history
    )
    reply_content: str = completion.choices[0].message.content
    logging.info("generating blog complete!")
    return reply_content


def main():
    questions = [
        "What topics do you want to write about?",
        "Any questions you want the blog post to answer?",
        "Any keys words you would like to include in the blog post?",
        "How many words should this blog post be?",
    ]
    answers = [
        "I want to write about camping",
        "what to bring for winter camping? what to eat for winter camping? what are the essentials to bring?",
        "ten things to bring for winter camping, things to wear for winter camping",
        "250"
    ]
    content = "I have my clients anwswering the following questions: " + "\n".join(questions) + "."
    content += "and here are their responses respectively: " + "\n".join(answers) + "."
    content += "Based on these information, can you generate a blog in markdown format?"
    message = [
        {"role": "system", "content": "You are an expert blog writer"},
        {"role": "user", "content": content}
    ]
    print(content)
    res = chatgpt(message)
    print(res)


if __name__ == "__main__":
    main()
