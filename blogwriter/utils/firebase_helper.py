from functools import wraps
import logging
from flask import request, jsonify
from firebase_admin import auth, _auth_utils


def get_user(id_token: str):
    decoded_token = auth.verify_id_token(id_token)
    uid = decoded_token['uid']
    user = None
    try:
        user = auth.get_user(uid)
        logging.info('Successfully fetched user data: {0}'.format(user.uid))
    except _auth_utils.UserNotFoundError:
        logging.error("Login failed, User not found")

    return user


def jwt_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            token = decode_jwt_from_headers()
            user = get_user(token)
            if not user:
                return jsonify({"response": "user not found"}), 500
        except Exception:
            return jsonify({"response": "failed"}), 500
        return func(user, *args, **kwargs)
    return wrapper


def decode_jwt_from_headers() -> str:
    bearer = request.headers.get("Authorization")

    return bearer.split()[1]
