import gitlab
import logging


def commit(content, filename, project_namespace, private_token):
    gl = gitlab.Gitlab(private_token=private_token)
    # Get a project by name with namespace
    project = gl.projects.get(project_namespace)
    logging.info(f"Writing to project: {project.name}")
    data = {
        'branch': 'main',
        'commit_message': f'adding a new file: {filename}',
        'actions': [
            {
                'action': 'create',
                'file_path': filename,
                'content': content
            },
        ]
    }

    project.commits.create(data)


def get_repo_directories(token, project_namespace):
    gl = gitlab.Gitlab(private_token=token)
    project = gl.projects.get(project_namespace)
    repo_tree = project.repository_tree(recursive=True, all=True)
    directories = []
    for node in repo_tree:
        if node["type"] == "tree":
            directories.append(node["path"] + "/")
    return directories


def main():
    token = "glpat-4fDc4p2HhphNVWxLxhHr"
    get_repo_directories(token, "wuyuan_chen/camping_blog2")



if __name__ == "__main__":
    main()
