import logging
from flask import (
    Blueprint,
    request,
    jsonify,
)
from firebase_admin._user_mgt import UserRecord
from sqlalchemy import exc
from blogwriter import db
from blogwriter.models.user import User
from blogwriter.utils.firebase_helper import jwt_required


bp = Blueprint('auth', __name__)


@bp.post("/login")
@jwt_required
def login(firebase_user: UserRecord):
    logging.info("reach endpoint login.")
    email: str = firebase_user.email
    name: str = firebase_user.display_name
    user: User = User.query.filter_by(email=email).first()
    if not user:
        try:
            user = User(name, email)
            db.session.add(user)
            db.session.commit()
        except exc.IntegrityError as error:
            logging.error(f"error: {error}")
            return {"error": f"User {email} is already registered."}, 400  

    return {"res": "logged in"}, 201
