import json
from datetime import datetime
from flask import (
    Blueprint,
    request,
)
from firebase_admin._user_mgt import UserRecord
from blogwriter.utils.gitlab_helper import commit, get_repo_directories
from blogwriter.utils.firebase_helper import jwt_required


bp = Blueprint("publish", __name__)


@bp.post("/get_repo")
@jwt_required
def get_repo(user: UserRecord):
    req_data: dict = request.get_json()
    project_ns: str = req_data.get("project_ns")
    token: str = req_data.get("api_key")
    # import pdb; pdb.set_trace()
    print(token)
    print(project_ns)
    directories = get_repo_directories(token, project_ns)
    print(directories)
    return {"result": directories}, 200


@bp.post("/publish")
@jwt_required
def publish(user: UserRecord):
    req_data: dict = request.get_json()
    author: str = req_data.get("author")
    author = json.dumps(author)

    title: str = req_data.get("title")
    title = json.dumps(title)

    tags: str = req_data.get("tags")
    tags = json.dumps(tags.split(","))

    content: str = req_data.get("content")
    project_ns: str = req_data.get("project_ns")
    api_key: str = req_data.get("api_key")
    filename: str = req_data.get("filename")
    publish_time: str = json.dumps(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))

    header = f'+++\nauthor = {author}\ntitle = {title}\ndate = {publish_time}' \
        f'\ntags = {tags}\n+++\n'
    commit(header+content, filename, project_ns, api_key)
    return {"response": "success"}, 200
