from datetime import datetime
from blogwriter import db


class Post(db.Model):
    __tablename__ = "post"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    title = db.Column(db.String, unique=False, nullable=False)
    content = db.Column(db.Text, nullable=False)
    date_psoted = db.Column(db.DateTime, default=datetime.now)
