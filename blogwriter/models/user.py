from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import UserMixin

from blogwriter import db
from datetime import datetime


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String, unique=False, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now,
                           onupdate=datetime.now)
    # message = db.relationship("Message", backref='user')

    def __init__(self, name, email):
        self.name = name
        self.email = email

    def get(user_id: int):
        user = User.query.filter_by(id=user_id).first()
        if not user:
            return None
        return user

    def __repr__(self):
        return f"User('{self.name} {self.email}')"
