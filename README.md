## create database
step 1: run the shell:
> flask --app blogwriter shell

If 'blogwriter.sqlite' is not created already in the instance folder,
then run, (need to import the models inorder for sqlalchemy to create those tables):
> from blogwriter.models.user import User
> from blogwriter.models.post import Post
> from blogwriter import db
> db.create_all()

To add data into database:
> user1 = User(name="user1", email="user1@example.com", password="user1")
> user2 = User(name="user2", email="user2@example.com", password="user2")
> db.session.add(user1)
> db.session.add(user2)
> db.session.commit()

>user1 = User.query.filter_by(email="user1@example.com").first()
>post1 = Post(title="hello world", content="this is the first post", user_id=user1.id)
>db.session.add(post1)
>db.session.commit()


To query all users:
> User.query.all()

To query first user:
> User.query.first()

To user filter:
> User.query.filter_by(email="user1@user1.com").first()


Running the app using docker:
// build the image with tag name blogwriter for the current folder
> docker build -t blogwriter .

// run the docker image named blogwriter, map the docker internal port 5000 to port 5000 on the outside.
> docker run -d -p 5000:5000 blogwriter